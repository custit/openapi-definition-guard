<?php

declare(strict_types=1);

namespace CustIT\Tests\OpenAPIDefinitionGuardBundle\DependencyInjection;

use CustIT\OpenAPIDefinitionGuardBundle\DependencyInjection\OpenAPIDefinitionGuardExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;

final class OpenAPIDefinitionGuardExtensionTest extends AbstractExtensionTestCase
{
    protected function getContainerExtensions(): array
    {
        return [new OpenAPIDefinitionGuardExtension()];
    }

    public function testLoading(): void
    {
        $this->load();

        $this->assertContainerBuilderHasService('custit_test');
    }

    public function testCompile(): void
    {
        $this->expectNotToPerformAssertions();

        $this->load();

        $this->compile();
    }
}