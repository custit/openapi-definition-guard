<?php

declare(strict_types=1);

namespace CustIT\Tests\OpenAPIDefinitionGuardBundle\DependencyInjection;

use CustIT\OpenAPIDefinitionGuardBundle\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Processor;

final class ConfigurationTest extends TestCase
{
    public function testDefaultConfig(): void
    {
        $processor = new Processor();
        $config = $processor->processConfiguration(new Configuration(), []);

        self::assertEquals(self::getBundleDefaultConfig(), $config);
    }

    private static function getBundleDefaultConfig(): array
    {
        return [];
    }
}