<?php

declare(strict_types=1);

namespace CustIT\Tests\OpenAPIDefinitionGuardBundle\Command;

use CustIT\OpenAPIDefinitionGuardBundle\Command\OpenAPIGuardCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;

final class OpenAPIGuardCommandTest extends TestCase
{
    public function testDefinedRoutesCommand(): void
    {
        $tester = $this->createCommandTester($this->getRouterWithDefinedRoutes());

        self::assertSame(OpenAPIGuardCommand::SUCCESS, $tester->execute([]));
        self::assertSame('', trim($tester->getDisplay()));
    }

    public function testMissingRoutesCommand(): void
    {
        $tester = $this->createCommandTester($this->getRouterWithoutRoutes());

        self::assertSame(OpenAPIGuardCommand::SUCCESS, $tester->execute([]));
        self::assertSame(
            <<<TEXT
The following routes are not defined in your application:
- GET "/path"
- PUT "/path"
- POST "/path"
- DELETE "/path"
- OPTIONS "/path"
- HEAD "/path"
- PATCH "/path"
- GET "/path/{path_parameter}"
TEXT
            ,
            trim($tester->getDisplay())
        );
    }

    private function createCommandTester(RouterInterface $router): CommandTester
    {
        $application = new Application($this->getKernel($router));
        $application->add(new OpenAPIGuardCommand($router, __DIR__ . '/../Files/test-openapi-v3.yaml'));

        return new CommandTester($application->find(OpenAPIGuardCommand::getDefaultName()));
    }

    private function getKernel(RouterInterface $router): KernelInterface
    {
        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container
            ->expects(self::atLeastOnce())
            ->method('has')
            ->willReturnCallback(
                static function ($id) {
                    return 'console.command_loader' !== $id;
                }
            );
        $container
            ->method('get')
            ->with('router')
            ->willReturn($router);

        $kernel = $this->getMockBuilder(KernelInterface::class)->getMock();
        $kernel
            ->method('getContainer')
            ->willReturn($container);

        $kernel
            ->expects(self::once())
            ->method('getBundles')
            ->willReturn([]);

        return $kernel;
    }

    private function getRouterWithoutRoutes(): RouterInterface
    {
        $routeCollection = new RouteCollection();

        $requestContext = new RequestContext();

        $router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $router
            ->method('getRouteCollection')
            ->willReturn($routeCollection);
        $router
            ->method('getContext')
            ->willReturn($requestContext);

        return $router;
    }

    private function getRouterWithDefinedRoutes(): RouterInterface
    {
        $routeCollection = new RouteCollection();
        $routeCollection->add(
            'my_route1',
            new Route(
                '/path',
                [],
                [],
                [],
                '',
                [],
                [
                    'options',
                    'get',
                    'head',
                    'post',
                    'put',
                    'patch',
                    'delete',
                ]
            )
        );
        $routeCollection->add(
            'my_route2',
            new Route(
                '/path/{path_parameter}',
                [],
                [],
                [],
                '',
                [],
                [
                    'get',
                ]
            )
        );

        $requestContext = new RequestContext();

        $router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $router
            ->method('getRouteCollection')
            ->willReturn($routeCollection);
        $router
            ->method('getContext')
            ->willReturn($requestContext);

        return $router;
    }
}