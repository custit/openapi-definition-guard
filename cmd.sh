#!/usr/bin/env bash

function build() {
  docker build --build-arg UID="$(id -u)" --build-arg GID="$(id -g)" -f docker/Dockerfile -t openapi-definition-guard .
}

function run() {
  docker run --rm -it --env-file=docker/.env -v "$(pwd)":/var/www/project --name openapi-definition-guard -P openapi-definition-guard "$@"
}

function console() {
  run 'bin/console' "$@"
}

function psalm() {
  run 'vendor/bin/psalm' "$@"
}

function phpunit() {
  run 'vendor/bin/simple-phpunit' "$@"
}

function composer() {
  run composer "$@"
}

function check() {
  function runCommand() {
    echo -n "run $1 ... "

    # shellcheck disable=SC2068
    if $@ > /dev/null; then
      echo -e "\e[32;1mdone\e[0m";
    else
      echo -e "\e[31;1mfailed\e[0m";
    fi
  }

  runCommand psalm
  runCommand phpunit
}

$1 "${@:2}"
