<?php

declare(strict_types=1);

namespace CustIT\OpenAPIDefinitionGuardBundle\Command;

use cebe\openapi\spec\Operation;
use cebe\openapi\spec\PathItem;
use League\OpenAPIValidation\PSR7\RoutedServerRequestValidator;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;

final class OpenAPIGuardCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'custit:openapi:guard';
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var RoutedServerRequestValidator
     */
    private $validator;

    public function __construct(RouterInterface $router, string $file)
    {
        parent::__construct();

        $this->router = $router;
        $this->validator = (new ValidatorBuilder())->fromYamlFile($file)->getRoutedRequestValidator();
    }

    protected function configure(): void
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('')
            ->setDefinition([])
            ->setHelp('');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $definedInAPISpec = [];
        $missingInApplication = [];

        /**
         * @var string $uri
         * @var PathItem $path
         */
        foreach ($this->validator->getSchema()->paths as $uri => $path) {
            /**
             * @var string $operationName
             * @var Operation $operation
             */
            foreach ($path->getOperations() as $operationName => $operation) {
                $route = $this->matchRoute($uri, $operationName);

                if ($route === null) {
                    $missingInApplication[] = sprintf('- %s "%s"', strtoupper($operationName), $uri);
                } else {
                    $definedInAPISpec[] = $route;
                }
            }
        }

        if (count($missingInApplication) > 0) {
            $output->writeln('The following routes are not defined in your application:');
            foreach ($missingInApplication as $message) {
                $output->writeln($message);
            }
        }

        /** @var array<string, Route> $routes */
        $routes = $this->router->getRouteCollection();
        $missingInAPISpec = [];

        foreach ($routes as $routeName => $route) {
            if (!in_array($route, $definedInAPISpec, true)) {
                if (count($route->getMethods()) > 0) {
                    foreach ($route->getMethods() as $method) {
                        $missingInAPISpec[] = sprintf('- "%s" (%s: "%s")', $routeName, $method, $route->getPath());
                    }
                } else {
                    $missingInAPISpec[] = sprintf('- "%s" ("%s")', $routeName, $route->getPath());
                }
            }
        }

        if (count($missingInAPISpec) > 0) {
            $output->writeln('The following routes are not defined in your OpenAPI specification:');
            foreach ($missingInAPISpec as $message) {
                $output->writeln($message);
            }
        }

        return Command::SUCCESS;
    }

    private function matchRoute(string $uri, string $method): ?Route
    {
        /** @var array<string, Route> $routes */
        $routes = $this->router->getRouteCollection();

        foreach ($routes as $routeName => $route) {
            $routeMethods = $route->getMethods();
            $routeUri = $route->getPath();

            if ($routeUri !== $uri || count($routeMethods) !== 1 || strtolower($routeMethods[0] ?? '') !== $method) {
                continue;
            }

            return $route;
        }

        return null;
    }
}