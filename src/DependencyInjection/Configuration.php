<?php

declare(strict_types=1);

namespace CustIT\OpenAPIDefinitionGuardBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('custit_openapi_definition_guard');

        $treeBuilder
            ->getRootNode()
            ->end();

        return $treeBuilder;
    }
}