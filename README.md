# OpenAPIDefinitionGuardBundle
[![pipeline status](https://gitlab.com/custit/openapi-definition-guard/badges/main/pipeline.svg)](https://gitlab.com/custit/openapi-definition-guard/-/commits/main)
[![coverage report](https://gitlab.com/custit/openapi-definition-guard/badges/main/coverage.svg)](https://gitlab.com/custit/openapi-definition-guard/-/commits/main)

## Installation

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Applications that use Symfony Flex
----------------------------------

Open a command console, enter your project directory and execute:

```console
$ composer require openapi-definition-guard-bundle
```

Applications that don't use Symfony Flex
----------------------------------------

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```shell
$ composer require openapi-definition-guard-bundle
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    CustIT\OpenAPIDefinitionGuardBundle::class => ['all' => true],
];
```

## Development
### Build docker image
```shell
$ ./cmd.sh build
```

### Exec docker container
```shell
$ ./cmd.sh run sh
```

### Run psalm
```shell
$ ./cmd.sh psalm
```

### Run tests
```shell
$ ./cmd.sh phpunit
```
